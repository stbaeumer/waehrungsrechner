const express = require('express')
const request = require('request');
const app = express()
app.set('view engine', 'ejs')
app.use(express.static('public'))
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended: true}))
const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})

app.get('/',(req, res, next) => {    
    res.render('index.ejs', {                    
        anzeigen: "Umrechnen"        
    })
})

app.post('/', (req, res) => {
    
    let fremdwaehrung = req.body.fremdwaehrung
    let menge = req.body.menge

    request('https://free.currencyconverterapi.com/api/v6/convert?q=' + fremdwaehrung + '_EUR&compact=ultra&apiKey=6a5180b6d6ef8f087e36', { json: true }, (err, response) => {        

        let wechselkurs = eval('response.body.' + fremdwaehrung + '_EUR')
        
        console.log(menge)
        console.log(wechselkurs)

        let result = menge + " " + fremdwaehrung + " entsprechen " + (menge * wechselkurs) + " EUR" 

        res.render('index.ejs', {                    
            anzeigen: result
        })
    })  
})
